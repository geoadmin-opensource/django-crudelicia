CRUDelicia
=====

Criação de CRUDs genéricos.


Para utilizar:
* Criar o CrudMeta no Model
* Criar o SearchForm (herdando de crudelicia.forms.SearchForm)
* Importar crudelicia.views.CrudViews nas views e criar com os parâmetros necessários.
* Nas urls, utilizar o .add_urls do crud criado.


Para exemplos, ver o test/testapp

Para rodar os testes: ./runtests.py (em /test)
