# coding: utf-8
from django import forms


def build_model_form(model):
    """ Cria um ModelForm genérico para um Model """

    meta = type('Meta', (object,), dict(model=model))
    return type(
        "{0}Form".format(model.__name__),
        (forms.ModelForm,),
        dict(Meta=meta)
    )


def without_self(dic):
    """ Retorna uma cópida de um dicionário sem a chave 'self' """

    ndic = dic.copy()
    ndic.pop('self')
    return ndic


def decorate(func, decorators):
    """ Recebe uma função e uma lista de decorators,
        retorna a função resultante de se aplicar os decorators
        à essa funçao. """

    try:
        iterator = iter(decorators)
    except TypeError:
        iterator = [decorators]

    for d in iterator:
        func = d(func)

    return func


def permission_name(namespace, codename):
    """ Retorna o nome de uma permissão dados o namespace e o codename """

    return namespace and '{0}.{1}'.format(namespace, codename) or codename
