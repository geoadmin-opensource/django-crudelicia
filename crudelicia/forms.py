# coding: utf-8
from django import forms
from django.contrib.gis.db import models


class TrimmedCharFormField(forms.CharField):

    """ CharField que dá trim nos valores antes do clean """

    def clean(self, value):
        if value:
            value = value.strip()
        return super(TrimmedCharFormField, self).clean(value)


class SearchForm(forms.Form):

    """ Formulário de pesquisa genérico """

    def _to_param(self, cleaned_value):
        if isinstance(cleaned_value, models.Model):
            return cleaned_value.pk
        else:
            return cleaned_value

    def extract_filter_data(self):
        """ Extrai dados para filtragem a partir de um dicionário
            self.search_params que deve ser implementado """

        if self.is_valid():

            params = {}

            for key in self.search_params:

                if self.cleaned_data[self.search_params[key]]:

                    params[key] = self._to_param(
                        self.cleaned_data[self.search_params[key]])

            return params

        return None

    def search_str(self):
        if self.is_valid():
            return '&'.join(
                ['{}={}'.format(key, self._to_param(self.cleaned_data[key]))
                 for key in self.cleaned_data if self.cleaned_data[key]]
            )
        else:
            return ''
