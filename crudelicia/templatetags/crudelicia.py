# coding: utf-8
from django import template
from django.contrib.gis.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.fields import FieldDoesNotExist

import copy

register = template.Library()


@register.filter
def getattribute(obj, attr_name):
    """ Retorna o atributo com nome 'attr_name' do objeto 'obj' """

    if hasattr(obj, str(attr_name)):
        return getattr(obj, str(attr_name))
    elif str(attr_name) in obj:
        return obj[str(attr_name)]
    else:
        return None


@register.filter
def model_verbose_name(obj):
    """ Retorna o verbose_name do model do objeto """

    return obj._meta.verbose_name.title()


@register.filter
def verbose_field_name(obj, field_name):
    """ Retorna o verbose_name de um campo do objeto """

    try:
        field = next(x for x in obj._meta.fields if x.name == field_name)
    except StopIteration:
        return field_name.title()

    return field.verbose_name


@register.filter
def detail_url(obj):
    return obj.CrudMeta._crudelicia_detail_url


@register.filter
def detail_fields(obj):
    """ Retorna a lista de campos a serem utilizados nos detalhes """

    return obj.CrudMeta.detail_fields


@register.filter
def list_fields(obj):
    """ Retorna a lista de campos a serem utilizados na listagem """

    return obj.CrudMeta.list_fields


@register.filter()
def with_user(obj, user):

    obj2 = copy.copy(obj)
    setattr(obj2, '_user', user)
    return obj2


@register.filter()
def detail(obj, field_name):
    """ Retorna o html do detalhe de um campo do objeto """

    value = getattr(obj, field_name)

    try:
        field = obj._meta.get_field(field_name)
    except FieldDoesNotExist:
        field_html = value
        name = field_name.replace('_', ' ').title()
    else:
        field_html = _field_html(field, value, obj._user)
        name = field.verbose_name

    return u"""<tr>
                 <td><strong>{0}</strong></td>
                 <td>{1}</td>
               </tr>""".format(name, field_html)


@register.filter
def is_multipart(form):
    """ Indica se um form é multipart """

    return form.is_multipart()


def _field_html(field, value, user):
    """ Cria o HTML do valor de um campo para exibição nos detalhes,
        conforme o tipo daquele campo.
    """

    if not value and not field.__class__ == models.BooleanField:
        return '-'

    if (field.__class__ == models.CharField
            or field.__class__ == models.IntegerField
            or field.__class__ == models.DecimalField
            or field.__class__ == models.AutoField):
        return value

    if field.__class__ == models.URLField:
        return u"<a href='{0}'>{1}</a>".format(value, value)

    if field.__class__ == models.FloatField:
        return "{:10.2f}".format(value)

    if field.__class__ == models.TextField:
        return u"<pre>{0}</pre>".format(value)

    if field.__class__ == models.BooleanField:
        if value:
            return ('<span style="color:green;" '
                    'class="glyphicon glyphicon-ok">'
                    '</span>')
        else:
            return ('<span style="color:red;" '
                    'class="glyphicon glyphicon-remove"></span>')

    if field.__class__ == models.FileField:
        filename = str(value)
        basename = filename.split('/')[-1]
        return (u"<a href='{0}{1}'>{2}</a>"
                .format(settings.MEDIA_URL, filename, basename))

    if field.__class__ == models.ForeignKey:

        related_model = field.rel.to

        if (hasattr(related_model, 'CrudMeta')
                and user.has_perm(related_model.CrudMeta.read_permission)):
            return (
                u"<a href='{0}'>{1}</a>"
                .format(
                    reverse(
                        related_model.CrudMeta._crudelicia_detail_url,
                        kwargs={'id': value.pk}
                    ),
                    value
                )
            )

        else:
            return str(value)

    if field.__class__ == models.DateField:
        return '{0:02d}/{1:02d}/{2:04d}'.format(value.day, value.month, value.year)

    raise NotImplementedError(u"[crudelicia] HTML do campo não implementado "
                              u"para {0}.".format(field.__class__))
