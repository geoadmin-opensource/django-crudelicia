# coding: utf-8
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from crudelicia import util


class CrudViews(object):

    """ Classe que gera as views de edit, create, update, search e detail
            para um model. """

    def __init__(self, model, search_form, base_template,
                 model_form=None, namespace=None, view_config={}):
        """ model:         Model para o qual o CRUD será feito.
                           O model deve possuir uma classe CrudMeta
                           especificando as seguintes opções:
                              detail_fields:   lista com nome dos campos que
                                               aparecerão nos detalhes.

                              list_fields:     lista com o nome dos campos que
                                               aparecerão nas listagens.

                              read_permission: nome da permissão de leitura

            base_template: Template base a ser utilizado pelas views
                           do CRUD.

            search_form:   Formulário de pesquisa, deve derivar de
                           crudelicia.forms.SearchForm, pois precisa
                           responder a extract_filter_data()

            model_form:    Formulário para criação e edição. Se não for
                           passado, um formulário padrão é criado.

            namespace:     Namespace das urls do CRUD, caso haja.

            view_config:   Dicionário com configurações especiais para as
                           views. As chaves devem ser os nomes das views para
                           as quais se quer editar as configurações:
                           'edit', 'search', 'create', 'edit' e 'detail'.
                           Os valores podem ser:
                             False: Caso não se queira aquela view.
                             ou
                             string: Para mudar o nome da view
                             ou
                             Um dicionário que pode ter as seguintes chaves:
                                'name':       para alterar o nome da view
                                'template':   para alterar o template utilizado
                                              pela view
                                'decorators': lista com decorators a serem
                                              utilizados pela view
            """

        self.model = model
        self.model_form = model_form or util.build_model_form(model)
        self.search_form = search_form
        self.namespace = namespace

        self.base_template = base_template

        # Valores padrão para configuração das views:
        self.view_config = {
            'create': {'name': 'criar',
                       'template': 'crudelicia/create_update.html',
                       'decorators': [self._add_permission, login_required]},

            'detail': {'name': 'detalhe',
                       'template': 'crudelicia/detail.html',
                       'decorators': [self._read_permission, login_required]},

            'search': {'name': 'pesquisar',
                       'template': 'crudelicia/search.html',
                       'decorators': [self._read_permission, login_required]},

            'delete': {'name': 'excluir',
                       'template': 'crudelicia/delete.html',
                       'decorators': [self._delete_permission,
                                      login_required]},

            'edit':   {'name': 'editar',
                       'template': 'crudelicia/create_update.html',
                       'decorators': [self._change_permission,
                                      login_required]},
        }

        for key in view_config:
            if key not in self.view_config:
                raise KeyError("[crudelicia] There's no view named '{0}' "
                               "in CrudViews.".format(key))

            # Se for False, apaga a view
            if view_config[key] is False:
                del(self.view_config[key])

            # Se for uma string, altera o nome
            elif isinstance(view_config[key], str):
                self.view_config[key]['name'] = view_config[key]

            # Se for um dic, altera as opções passadas
            elif isinstance(view_config[key], dict):
                for option in view_config[key]:
                    if option not in self.view_config[key]:
                        raise KeyError(u"[crudelicia] There's no option '{0}' "
                                       u"for a view.".format(option))

                    self.view_config[key][option] = view_config[key][option]
            else:
                raise TypeError(u"[crudelicia] Unrecognized type "
                                u"for view_config options.")

        model.CrudMeta._crudelicia_detail_url = self._view_url('detail')

    def add_urls(self, patterns):
        """ Recebe um pattern e adiciona nele as urls do CRUD """

        if 'create' in self.view_config:
            patterns.append(url(r"^{0}/$".format(self._view_name('create')),
                                self.create, name=self._view_name('create')))

        if 'search' in self.view_config:
            patterns.append(url(r"^{0}/$".format(self._view_name('search')),
                                self.search, name=self._view_name('search')))

        if 'detail' in self.view_config:
            patterns.append(url(r"^{0}/(?P<id>\d+)/$"
                                .format(self._view_name('detail')),
                                self.detail,
                                name=self._view_name('detail')))

        if 'edit' in self.view_config:
            patterns.append(url(r"^{0}/(?P<id>\d+)/$"
                                .format(self._view_name('edit')),
                                self.edit,
                                name=self._view_name('edit')))

        if 'delete' in self.view_config:
            patterns.append(url(r"^{0}/(?P<id>\d+)/$"
                                .format(self._view_name('delete')),
                                self.delete,
                                name=self._view_name('delete')))

    def create_context(self, obj_form, *args, **kwargs):
        """ Retorna o contexto para a view 'create'.
            Pode-se sobrescrever para alterar o contexto.
        """

        return {"obj_form": obj_form,
                "title": self.model_name,
                "subtitle": 'criar',
                "base_template": self.base_template}

    def before_create(self, obj, *args, **kwargs):
        """ Função executada antes de salvar o objeto duranto o create.
            Pode ser sobrescrita para adicionar comportamento extra.
        """

        return obj

    def create(self, request):
        """ View de criação """

        def create_view_func(request):
            """ Função básica da view, que será decorada
                com os decorators corretos.
            """

            obj_form = self.model_form(request.POST or None,
                                       request.FILES or None)

            if obj_form.is_valid():
                obj = obj_form.save(commit=False)
                obj = self.before_create(**util.without_self(locals()))
                obj.save()
                messages.success(request,
                                 u"{0} '{1}'' cadastrado com sucesso."
                                 .format(self.model_name.title(), obj))

                return redirect(self._view_url('detail'), **{"id": obj.id})

            else:
                return render(
                    request,
                    self._view_template('create'),
                    self.create_context(**util.without_self(locals()))
                )

        func = util.decorate(create_view_func, self._view_decorators('create'))
        return func(request)

    def detail_context(self, obj, *args, **kwargs):
        """ Retorna o contexto para a view 'detail'.
            Pode-se sobrescrever para alterar o contexto.
        """

        return {"instance": obj,
                "base_template": self.base_template,
                "search_view": self._view_url('search')}

    def detail(self, request, id):
        """ View de detalhe """

        def detail_view_func(request, id):
            """ Função básica da view, que será decorada
                com os decorators corretos.
            """

            obj = get_object_or_404(self.model, pk=id)
            return render(
                request,
                self._view_template('detail'),
                self.detail_context(**util.without_self(locals()))
            )

        func = util.decorate(detail_view_func, self._view_decorators('detail'))
        return func(request, id)

    def search_context(self, obj_search_form, objs, *args, **kwargs):
        """ Retorna o contexto para a view 'search'.
            Pode-se sobrescrever para alterar o contexto.
        """

        context = {"model": self.model,
                   "search_form": obj_search_form,
                   "objects": objs,
                   "base_template": self.base_template,
                   "title": self.model_name,
                   "subtitle": 'Pesquisar', }

        if 'delete' in self.view_config:
            context["delete_view"] = self._view_url('delete')

        if 'detail' in self.view_config:
            context["detail_view"] = self._view_url('detail')

        return context

    def search(self, request):
        """ View de pesquisa """

        def search_view_func(request):
            """ Função básica da view, que será decorada
                com os decorators corretos.
            """

            obj_search_form = self.search_form(request.GET or None)

            if obj_search_form.is_valid():
                params = obj_search_form.extract_filter_data()
            else:
                params = None

            if params is not None:
                obj_list = self.model.objects.filter(**params)
            else:
                obj_list = self.model.objects.all()

            page = request.GET.get('pag')

            paginator = Paginator(obj_list, 25)
            try:
                objs = paginator.page(page)
            except PageNotAnInteger:
                objs = paginator.page(1)
            except EmptyPage:
                objs = paginator.page(paginator.num_pages)

            return render(
                request,
                self._view_template('search'),
                self.search_context(**util.without_self(locals()))
            )

        func = util.decorate(search_view_func, self._view_decorators('search'))
        return func(request)

    def edit_context(self, obj_form, *args, **kwargs):
        """ Retorna o contexto para a view 'edit'.
            Pode-se sobrescrever para alterar o contexto.
        """

        return {"obj_form": obj_form,
                "title": self.model_name,
                "subtitle": 'Editar',
                "base_template": self.base_template}

    def before_edit(self, obj, *args, **kwargs):
        """ Função executada antes de salvar o objeto duranto o edit.
            Pode ser sobrescrita para adicionar comportamento extra.
        """

        return obj

    def edit(self, request, id):
        """ View de edição """

        def edit_view_func(request, id):
            """ Função básica da view, que será decorada
                com os decorators corretos.
            """

            obj = get_object_or_404(self.model, pk=id)
            obj_form = self.model_form(request.POST or None, instance=obj)

            if obj_form.is_valid():
                obj = obj_form.save(commit=False)
                obj = self.before_edit(**util.without_self(locals()))
                obj.save()
                messages.success(
                    request,
                    u"{0} '{1}'' atualizado com sucesso."
                    .format(self.model_name.title(), obj)
                )
                return redirect(self._view_url('detail'), **{"id": obj.id})
            else:
                return render(
                    request,
                    self._view_template('edit'),
                    self.edit_context(**util.without_self(locals()))
                )

        func = util.decorate(edit_view_func, self._view_decorators('edit'))
        return func(request, id)

    def delete_context(self, obj, *args, **kwargs):
        """ Retorna o contexto para a view 'delete'.
            Pode-se sobrescrever para alterar o contexto.
        """

        return {"obj": obj,
                "search_view": self._view_url('search'),
                "model_name":  self.model_name,
                "base_template": self.base_template}

    def delete(self, request, id):
        """ View de exclusão """

        def delete_view_func(request, id):
            """ Função básica da view, que será
                decorada com os decorators corretos.
            """

            obj = get_object_or_404(self.model, pk=id)

            if request.method == "POST":

                messages.success(
                    request,
                    u"{0} '{1}' excluído com sucesso."
                    .format(self.model_name.title(), obj)
                )
                obj.delete()

                return redirect(self._view_url('search'))

            else:
                return render(
                    request,
                    self._view_template('delete'),
                    self.delete_context(**util.without_self(locals()))
                )

        func = util.decorate(delete_view_func, self._view_decorators('delete'))
        return func(request, id)

    @property
    def model_name(self):
        """ Nome do model relacionado. """

        return self.model._meta.verbose_name

    @property
    def _read_permission(self):
        """ Nome da permissão de leitura dos objetos do model relacionado """

        return permission_required(
            util.permission_name(
                self.namespace, self.model.CrudMeta.read_permission
            )
        )

    @property
    def _add_permission(self):
        """ Nome da permissão de criação dos objetos do model relacionado """

        return permission_required(
            util.permission_name(
                self.namespace, self.model._meta.get_add_permission()
            )
        )

    @property
    def _change_permission(self):
        """ Nome da permissão de modificação dos objetos
            do model relacionado
        """

        return permission_required(
            util.permission_name(
                self.namespace, self.model._meta.get_change_permission()
            )
        )

    @property
    def _delete_permission(self):
        """ Nome da permissão de exclusão dos objetos do model relacionado """

        return permission_required(
            util.permission_name(
                self.namespace, self.model._meta.get_delete_permission()
            )
        )

    def _view_url(self, view):
        """ Retorna o nome da url de uma das views do CRUD """

        if self.namespace:
            return '{0}:{1}'.format(self.namespace, self._view_name(view))
        else:
            return self._view_name(view)

    def _view_name(self, view):
        """ Retorna o nome de uma das views do CRUD """

        return self.view_config[view]['name']

    def _view_template(self, view):
        """ Retorna o template de uma das views do CRUD """

        return self.view_config[view]['template']

    def _view_decorators(self, view):
        """ Retorna os decorators de uma das views do CRUD """

        return self.view_config[view]['decorators']
