#!/usr/bin/env python
import os
import sys

DIRNAME = os.path.dirname(__file__)
os.chdir(DIRNAME + "/../../")
sys.path[0] = os.getcwd()

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "crudelicia.test.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(['', 'test', 'crudelicia.test.testapp'])
