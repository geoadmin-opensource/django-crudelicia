from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',

    url(r"^crudtest/",
        include("crudelicia.test.testapp.urls", namespace="crudtest")
        ),

    url(r"^customtest/",
        include("crudelicia.test.testapp.urls_custom", namespace="customtest")
        ),

    url(r"^decotest/",
        include("crudelicia.test.testapp.urls_deco", namespace="decotest")
        ),
)
