from django.db import models
from django.contrib.auth.models import User


class Starship(models.Model):

    name = models.CharField(max_length=128, unique=True)

    serial_number = models.CharField(max_length=128, unique=True)

    created_by = models.ForeignKey(User,
                                   related_name='created_by',
                                   blank=True,
                                   null=True)

    edited_by = models.ForeignKey(User,
                                  related_name='edited_by',
                                  blank=True,
                                  null=True)

    permissions = (
        ("read_starship", "Can read starship information"),
    )

    class CrudMeta:

        detail_fields = ['name', 'serial_number']
        list_fields = ['name', 'serial_number']
        read_permission = 'read_starship'
