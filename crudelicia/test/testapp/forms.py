# coding: utf-8
from crudelicia import forms as crudforms


class StarshipSearchForm(crudforms.SearchForm):

    name = crudforms.TrimmedCharFormField(max_length=128,
                                          required=False)

    serial = crudforms.TrimmedCharFormField(max_length=128,
                                            required=False)

    search_params = {
        'name__icontains': 'name',
        'serial_number__icontains': 'serial'
    }
