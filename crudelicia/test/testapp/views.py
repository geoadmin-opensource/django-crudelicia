from crudelicia.views import CrudViews
from crudelicia.test.testapp.models import Starship
from crudelicia.test.testapp.forms import StarshipSearchForm


crud = CrudViews(model=Starship,
                 search_form=StarshipSearchForm,
                 namespace='crudtest',
                 base_template='testeapp/base.html')


class CustomViews(CrudViews):

    def before_create(self, obj, request, **args):
        obj = super(CustomViews, self).before_create(obj, **args)
        obj.created_by = request.user
        return obj

    def before_edit(self, obj, request, **args):
        obj = super(CustomViews, self).before_edit(obj, **args)
        obj.edited_by = request.user
        return obj

    def create_context(self, **args):
        context = super(CustomViews, self).create_context(**args)
        context['custom_create_context'] = 'custom'
        return context

    def search_context(self, **args):
        context = super(CustomViews, self).search_context(**args)
        context['custom_search_context'] = 'custom'
        return context

    def edit_context(self, **args):
        context = super(CustomViews, self).edit_context(**args)
        context['custom_edit_context'] = 'custom'
        return context

    def delete_context(self, **args):
        context = super(CustomViews, self).delete_context(**args)
        context['custom_delete_context'] = 'custom'
        return context

    def detail_context(self, **args):
        context = super(CustomViews, self).detail_context(**args)
        context['custom_detail_context'] = 'custom'
        return context


custom_crud = CustomViews(model=Starship,
                          search_form=StarshipSearchForm,
                          namespace='customtest',
                          base_template='testeapp/base.html',
                          view_config={
                              'edit': {
                                  'name': 'custom_edit',
                                  'template': 'testeapp/hello.html'
                              },
                              'create': {
                                  'name': 'custom_create',
                                  'template': 'testeapp/hello.html'
                              },
                              'delete': {
                                  'name': 'custom_delete',
                                  'template': 'testeapp/hello.html'
                              },
                              'search': {
                                  'name': 'custom_search',
                                  'template': 'testeapp/hello.html'
                              },
                              'detail': {
                                  'name': 'custom_detail',
                                  'template': 'testeapp/hello.html'
                              },
                          })


def custom_decorator(func):
    def decorated(request, *args, **kwargs):
        response = func(request, *args, **kwargs)
        response.status_code = 666
        return response

    return decorated


deco_crud = CrudViews(model=Starship,
                      search_form=StarshipSearchForm,
                      namespace='decotest',
                      base_template='testeapp/base.html',
                      view_config={
                          'edit': {
                              'name': 'custom_edit',
                              'decorators': custom_decorator
                          },
                          'create': {
                              'name': 'custom_create',
                              'decorators': [custom_decorator]
                          },
                          'delete': {
                              'name': 'custom_delete',
                              'decorators': custom_decorator
                          },
                          'search': {
                              'name': 'custom_search',
                              'decorators': custom_decorator
                          },
                          'detail': {
                              'name': 'custom_detail',
                              'decorators': custom_decorator
                          },
                      })
