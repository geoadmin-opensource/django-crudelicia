# coding: utf-8
from django.core.urlresolvers import reverse
from django.test import TransactionTestCase, Client
from django.contrib.auth.models import User
from model_mommy import mommy
from crudelicia.test.testapp.models import Starship

USERNAME = "kirk"
PASSWORD = "captain"


class CreateView(TransactionTestCase):

    def setUp(self):

        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

    def test_create_get(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        response = c.get(reverse("crudtest:criar"))

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed("crudelicia/create_update.html")

    def test_create_get_not_logged_in(self):

        c = Client()
        response = c.get(reverse("crudtest:criar"))
        self.assertEqual(302, response.status_code)

    def test_create_post(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        response = c.post(reverse("crudtest:criar"),
                          data={"name": "Millenium Falcon",
                                "serial_number": "07812341666"},
                          follow=True)

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed("crudelicia/detail.html")
        self.assertEqual(1, Starship.objects.all().count())

    def test_create_post_not_logged_in(self):

        c = Client()
        response = c.post(reverse("crudtest:criar"),
                          data={"name": "Millenium Falcon",
                                "serial_number": "07812341666"})
        self.assertEqual(302, response.status_code)


class EditView(TransactionTestCase):

    def setUp(self):

        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

        starship = mommy.make(Starship,
                              name="SS Enterprise",
                              serial_number='NCC-1701')
        starship.save()

    def test_edit_get(self):

        c = Client()
        c.login(username=USERNAME, password=PASSWORD)

        starship = Starship.objects.all()[0]

        response = c.get(reverse("crudtest:editar",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.status_code, 200)

    def test_edit_get_not_logged_in(self):
        c = Client()

        starship = Starship.objects.all()[0]

        response = c.get(reverse("crudtest:editar",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.status_code, 302)

    def test_edit_get_404(self):

        c = Client()
        c.login(username=USERNAME, password=PASSWORD)

        response = c.get(reverse("crudtest:editar", kwargs={"id": 666}))

        self.assertEqual(response.status_code, 404)

    def test_edit_post(self):

        c = Client()
        c.login(username=USERNAME, password=PASSWORD)

        starship = Starship.objects.all()[0]

        self.assertEqual(starship.name, 'SS Enterprise')

        newname = 'USS Enterprise'
        response = c.post(reverse("crudtest:editar",
                                  kwargs={"id": starship.id}),
                          data={"name": newname,
                                "serial_number": starship.serial_number},
                          follow=True)

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed("crudelicia/detail.html")

        starship = Starship.objects.get(pk=starship.id)
        self.assertEqual(newname, starship.name)

    def test_edit_post_not_logged_in(self):

        c = Client()

        starship = Starship.objects.all()[0]
        oldname = 'SS Enterprise'
        self.assertEqual(starship.name, oldname)

        response = c.post(reverse("crudtest:editar",
                                  kwargs={"id": starship.id}),
                          data={"name": 'USS Enterprise',
                                "serial_number": starship.serial_number})

        self.assertEqual(302, response.status_code)

        starship = Starship.objects.get(pk=starship.id)
        self.assertEqual(oldname, starship.name)


class DetailView(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

    def test_detail(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        starship = Starship.objects.create(name='USS Enterprise',
                                           serial_number="NCC-1701")

        response = c.get(reverse("crudtest:detalhe",
                                 kwargs={"id": starship.id}))

        self.assertEqual(200, response.status_code)

    def test_detail_not_logged_in(self):

        c = Client()

        starship = Starship.objects.create(name='USS Enterprise',
                                           serial_number="NCC-1701")

        response = c.get(reverse("crudtest:detalhe",
                                 kwargs={"id": starship.id}))

        self.assertEqual(302, response.status_code)

    def test_detail_404(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        response = c.get(reverse("crudtest:detalhe", kwargs={"id": 666}))

        self.assertEqual(404, response.status_code)


class SearchView(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

        falcon = mommy.make(Starship,
                            name='Millenium Falcon',
                            serial_number='07812341666')
        falcon.save()

        enterprise = mommy.make(Starship,
                                name='USS Enterprise',
                                serial_number='NCC-1701')
        enterprise.save()

    def test_search_get(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        count = Starship.objects.all().count()

        response = c.get(reverse("crudtest:pesquisar"))
        in_response = len(response.context['objects'])
        self.assertEqual(count, in_response)

    def test_search_get_not_logged_in(self):

        c = Client()
        response = c.get(reverse("crudtest:pesquisar"))
        self.assertEqual(response.status_code, 302)

    def test_search_post_not_logged_in(self):

        c = Client()
        response = c.post(reverse("crudtest:pesquisar"),
                          {'name': 'enterprise'})
        self.assertEqual(response.status_code, 302)

    def test_blank_search(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        count = Starship.objects.all().count()

        response = c.post(reverse("crudtest:pesquisar"),
                          {'name': '  ', 'serial_number': ' '})
        in_response = len(response.context['objects'])
        self.assertEqual(count, in_response)

    def test_one_field_search(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        name = 'enterprise'

        response = c.post(reverse("crudtest:pesquisar"), {'name': name})

        in_response = len(response.context['objects'])

        self.assertEqual(
            Starship.objects.filter(name__icontains=name).count(),
            in_response
        )

    def test_two_field_search(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        name = 'enterprise'
        serial = '999'

        response = c.post(reverse("crudtest:pesquisar"),
                          {'name': name, 'serial': serial})

        in_response = len(response.context['objects'])
        count = (Starship
                 .objects.filter(name__icontains=name)
                 .filter(serial_number__icontains=serial)).count()

        self.assertEqual(count, in_response)


class DeleteView(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

    def test_delete_get(self):

        c = Client()
        starship = mommy.make(Starship, name="Millenium Falcon")
        c.login(username=USERNAME,
                password=PASSWORD)

        response = c.get(reverse("crudtest:excluir",
                                 kwargs={"id": starship.id}))

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed("crudelicia/delete.html")

    def test_delete_get_not_logged_int(self):

        c = Client()
        starship = mommy.make(Starship, name="Millenium Falcon")

        response = c.get(reverse("crudtest:excluir",
                                 kwargs={"id": starship.id}))

        self.assertEqual(302, response.status_code)

    def test_delete_post_not_logged_int(self):

        c = Client()
        starship = mommy.make(Starship, name="Millenium Falcon")

        response = c.post(reverse("crudtest:excluir",
                                  kwargs={"id": starship.id}))

        self.assertEqual(302, response.status_code)

    def test_delete_post(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        starship = mommy.make(Starship, name="Millenium Falcon")
        response = c.post(reverse("crudtest:excluir",
                                  kwargs={"id": starship.id}), follow=True)

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed("crudtest/search.html")
        self.assertEqual(0, Starship.objects.filter(id=starship.id).count())

    def test_delete_get_404(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        response = c.post(reverse("crudtest:excluir", kwargs={"id": 999}))

        self.assertEqual(404, response.status_code)
        self.assertEqual(0, Starship.objects.filter(id=999).count())


class CustomHookTest(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

    def test_custom_create_hook(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        c.post(reverse("customtest:custom_create"),
               data={"name": "Millenium Falcon",
                     "serial_number": "07812341666"},
               follow=True)

        starship = Starship.objects.all()[0]
        self.assertEqual(starship.created_by.username, USERNAME)

    def test_custom_edit_hook(self):

        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)

        starship = mommy.make(Starship,
                              name="USS Enterprise",
                              serial_number='NCC-1701')

        c.post(reverse("customtest:custom_edit", kwargs={"id": starship.id}),
               data={"name": starship.name,
                     "serial_number": starship.serial_number})

        starship = Starship.objects.get(pk=starship.id)
        self.assertEqual(starship.edited_by.username, USERNAME)


class CustomContextAndTemplateTest(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

        starship = mommy.make(Starship,
                              name="Millenium Falcon",
                              serial_number='1X')
        starship.save()

    def test_custom_create_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        response = c.get(reverse("customtest:custom_create"))

        self.assertEqual(response.context['custom_create_context'], 'custom')
        self.assertTemplateUsed("testapp/hello.html")

    def test_custom_search_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        response = c.get(reverse("customtest:custom_search"))

        self.assertEqual(response.context['custom_search_context'], 'custom')
        self.assertTemplateUsed("testapp/hello.html")

    def test_custom_detail_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("customtest:custom_detail",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.context['custom_detail_context'], 'custom')
        self.assertTemplateUsed("testapp/hello.html")

    def test_custom_edit_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("customtest:custom_edit",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.context['custom_edit_context'], 'custom')
        self.assertTemplateUsed("testapp/hello.html")

    def test_custom_delete_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("customtest:custom_delete",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.context['custom_delete_context'], 'custom')
        self.assertTemplateUsed("testapp/hello.html")


class CustomDecoratorTest(TransactionTestCase):

    def setUp(self):
        superuser = mommy.make(User,
                               username=USERNAME,
                               is_staff=True,
                               is_superuser=True)
        superuser.set_password(PASSWORD)
        superuser.save()

        starship = mommy.make(Starship,
                              name="Millenium Falcon",
                              serial_number='1X')
        starship.save()

    def test_custom_create_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        response = c.get(reverse("decotest:custom_create"))

        self.assertEqual(response.status_code, 666)

    def test_custom_search_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        response = c.get(reverse("decotest:custom_search"))

        self.assertEqual(response.status_code, 666)

    def test_custom_detail_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("decotest:custom_detail",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.status_code, 666)

    def test_custom_edit_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("decotest:custom_edit",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.status_code, 666)

    def test_custom_delete_context(self):
        c = Client()
        c.login(username=USERNAME,
                password=PASSWORD)
        starship = Starship.objects.all()[0]
        response = c.get(reverse("decotest:custom_delete",
                                 kwargs={"id": starship.id}))

        self.assertEqual(response.status_code, 666)
